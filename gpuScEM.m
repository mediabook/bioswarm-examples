//
// gpuScEM.m
//

#include <Foundation/Foundation.h>
#import <BioSwarm/BioSwarmManager.h>

extern void *getGPUFunctionForModel(int aModel, int value);

@interface MyDelegate : NSObject
{
}
@end
@implementation MyDelegate
@end

int scem(int my_rank)
{
  NSMutableDictionary *runParameters;
  NSAutoreleasePool *pool;
  NSString *paramFile;
  id s;

  pool = [NSAutoreleasePool new];

  BioSwarmManager *simManager = [[BioSwarmManager alloc] initWithDefaultModelFile: MODEL_FILE];
  id delegate = [MyDelegate new];
  [simManager setDelegate: delegate];

  if ([simManager processArguments: [[NSProcessInfo processInfo] arguments]]) {
    [simManager setGPUFunctions: &getGPUFunctionForModel];
    [simManager startSimulation];
  }

  [simManager release];
  [delegate release];

  [pool release];
  return 0;
}

int
main( int argc, char** argv) 
{
  scem(0);
}
