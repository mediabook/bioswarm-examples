//
// Simple test routines to check out GPU
//
#include <stdio.h>

void cudacheck(const char *message) {
  cudaError_t error = cudaGetLastError();
  if (error!=cudaSuccess) printf("cudaERROR: %s : %i (%s)\n", message, error, cudaGetErrorString(error));
}

void memoryCheck()
{
  int device = 0;
  cudaDeviceProp deviceProp;
  cudaGetDeviceProperties(&deviceProp, device);

  printf("Device %d has %ld total global memory\n", device, deviceProp.totalGlobalMem);

  size_t allocMem = deviceProp.totalGlobalMem * 0.9;
  void *devPtr;

  printf("Trying to allocate %ld memory\n", allocMem);
  cudaMalloc(&devPtr, allocMem);
  cudacheck("Memory allocation");

  cudaFree(devPtr);
  cudacheck("Memory allocation");
}

main()
{
  int deviceCount;
  cudaGetDeviceCount(&deviceCount);

  int device;
  for (device = 0; device < deviceCount; ++device) {
    cudaDeviceProp deviceProp;
    cudaGetDeviceProperties(&deviceProp, device);
    printf("Device %d: %s\n", device, deviceProp.name);
    printf("Compute capability %d.%d.\n", deviceProp.major, deviceProp.minor);
    printf("MultiProcessor count %d.\n", deviceProp.multiProcessorCount);
  }

  memoryCheck();
}
