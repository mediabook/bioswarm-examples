//
// gpuMCMC.m
//
// MCMC for Basic rule-based reaction model
//

#import <Foundation/Foundation.h>
#import <BioSwarm/ReactionRuleModel.h>
#import <BioSwarm/BioSwarmManager.h>

#import <BioCocoa/BCDataMatrix.h>
#ifdef GNUSTEP
#else
#import <Accelerate/Accelerate.h>
#endif

extern void *getGPUFunctionForModel(int aModel, int value);

#define OBS 12
//const int times[] = {0, 36000, 72000, 108000, 180000, 252000, 324000, 396000, 468000, 540000, 612000, 684000};
const int times[] = {0, 1800, 3600, 5400, 9000, 12600, 16200, 19800, 23400, 27000, 30600, 34200};
//const double rA[] = {0.072727, 0.057142, 0.047059, 0.040000, 0.034782, 0.030770, 0.027587, 0.025001, 0.022858, 0.021053};
//const double mA[] = {0.0726,    0.0567,    0.0481,    0.0400,    0.0346,    0.0313,    0.0283,    0.0245,    0.0226,    0.0208};
//const double mB[] = {0.0732,    0.0570,    0.0474,    0.0392,    0.0346,    0.0314,    0.0281,    0.0253,    0.0224,    0.0215};
//const double rC[] = {0.027273, 0.042857, 0.052941, 0.060000, 0.065216, 0.069227, 0.072411, 0.075007, 0.077140, 0.078957};
const double cA[] = {0.0985, 0.0637, 0.0500, 0.0462, 0.0363, 0.0248, 0.0171, 0.0168, 0.0131, 0.0150, 0.0140, 0.0134};
const double cB[] = {0.0995, 0.0651, 0.0596, 0.0453, 0.0384, 0.0247, 0.0174, 0.0203, 0.0136, 0.0121, 0.0142, 0.0134};
const double cC[] = {0.0010, 0.0357, 0.0501, 0.0512, 0.0682, 0.0747, 0.0809, 0.0818, 0.0858, 0.0863, 0.0872, 0.0928};

#define BINS 100

@interface MyDelegate : NSObject
{
  int totalParameters;
  NSArray *parameterNames;
  NSArray *parameterIndexes;
  NSArray *parameterModelNumbers;
  NSArray *parameterModelKeys;
  double *parameterMinRange;
  double *parameterMaxRange;
  void *acceptParametersValues;
  void *rejectParametersValues;

  int numOfParticles;
  BCDataMatrix *Y;
  BCDataMatrix *Y_hat;
  BCDataMatrix *Delta;
  BCDataMatrix *S;

  double N_below;
  double N_above;
  double hist[BINS];
}
- (void)didTransferDataToGPU: (BOOL)aFlag sender: (id)theController;

// MCMC delegate
- (void)parameterInfo: theController names: (NSArray *)names indexes: (NSArray *)indexes
	       modelNumbers: (NSArray *)modelNumbers modelKeys: (NSArray *)modelKeys
            minRanges: (double *)minRange maxRange: (double *)maxRange
         acceptValues: (void *)acceptValues rejectValues:(void *)rejectValues;
- (void)willCalculatePosteriorDensity: theController forIndividual: (int)modelIndex;
- (double)calculatePosteriorDensity: theController forIndividual: (int)modelIndex;
- (void)initExpectation: theController;
- (void)calculateExpectation: theController weights: (double *)particleWeights accept: (BOOL *)acceptFlags;
- (void)finishExpectation: theController;
@end

@implementation MyDelegate
- (void)didTransferDataToGPU: (BOOL)aFlag sender: (id)theController
{
}

//
// MCMC delegate
//

- (void)parameterInfo: theController names: (NSArray *)names indexes: (NSArray *)indexes
	       modelNumbers: (NSArray *)modelNumbers modelKeys: (NSArray *)modelKeys
            minRanges: (double *)minRange maxRange: (double *)maxRange
         acceptValues: (void *)acceptValues rejectValues:(void *)rejectValues
{
  printf("parameterInfo:\n");
  parameterNames = names;
  parameterIndexes = indexes;
  parameterModelNumbers = modelNumbers;
  parameterModelKeys = modelKeys;
  parameterMinRange = minRange;
  parameterMaxRange = maxRange;
  totalParameters = [parameterNames count];
  acceptParametersValues = acceptValues;
  rejectParametersValues = rejectValues;
  printf("parameterInfo: %d\n", totalParameters);
}

- (void)initExpectation: theController
{
  int i;
  for (i = 0; i < BINS; ++i) hist[i] = 0.0;
}

- (void)calculateExpectation: theController weights: (double *)particleWeights accept: (BOOL *)acceptFlags;
{
  printf("calculateExpectation:weights:accept:\n");

  double (*acceptParameters)[numOfParticles][totalParameters] = acceptParametersValues;
  double (*rejectParameters)[numOfParticles][totalParameters] = rejectParametersValues;

  NSString *pn = [parameterNames objectAtIndex: 0];
  double param;
  if (acceptFlags[0]) param = (*acceptParameters)[0][0];
  else param = (*rejectParameters)[0][0];

  double delta = (parameterMaxRange[0] - parameterMinRange[0]) / (double)BINS;
  int k = floor((param - parameterMinRange[0]) / delta);
  printf("%s = %f, %f, %d\n", [pn UTF8String], param, delta, k);

  if (k < 0)
    N_below += 1;
  else if (k >= BINS)
    N_above += 1;
  else
    hist[k] += 1;

  int i;
  FILE *out = fopen("MCMC_hist.dat", "a");
  for (i = 0; i < BINS; ++i) fprintf(out, "%f ", hist[i]);
  fprintf(out, "\n");
  fclose(out);
}

- (void)finishExpectation: theController
{
}

@end

int reaction(int my_rank)
{
  NSMutableDictionary *runParameters;
  NSAutoreleasePool *pool;
  NSString *paramFile;
  id s;

  pool = [NSAutoreleasePool new];

  BioSwarmManager *simManager = [[BioSwarmManager alloc] initWithDefaultModelFile: MODEL_FILE];
  id delegate = [MyDelegate new];
  [simManager setDelegate: delegate];

  if ([simManager processArguments: [[NSProcessInfo processInfo] arguments]]) {
    [simManager setGPUFunctions: &getGPUFunctionForModel];
    [simManager startSimulation];
  }

  [simManager release];
  [delegate release];

  [pool release];
  return 0;
}

#define SIZE 2

#ifndef GNUSTEP
void testInverse()
{
  int i;
    
  __CLPK_integer M = SIZE;
  __CLPK_integer N = SIZE;
  __CLPK_integer NRHS = SIZE;
    
  __CLPK_integer LDA = M;
  __CLPK_integer LDB = M;
  __CLPK_integer INFO;
  __CLPK_integer IPIVOT[SIZE];
   
  //__CLPK_integer mn = min( M, N );
    
  //__CLPK_integer MN = max( M, N );
     
  //double a[SIZE*SIZE] = { 16.0, 5.0, 9.0 , 4.0, 2.0, 11.0, 7.0 , 14.0, 3.0, 10.0, 6.0, 15.0, 13.0, 8.0, 12.0, 1.0};
  //double b[SIZE*SIZE] = { 1.0, 0, 0 , 0, 0, 1.0, 0 , 0, 0, 0, 1.0, 0, 0, 0, 0, 1.0};
  //double a[SIZE*SIZE] = {1.0, 4.0, 7.0, 2.0, 5.0, 8.0, 3.0, 6.0, 0.0};
  //double b[SIZE*SIZE] = {1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0};
  //double a[SIZE*SIZE] = {4.0, 6.0, 2.0, 0.0};
  double a[SIZE*SIZE] = {4.0, 2.0, 6.0, 0.0};
  //double a[SIZE*SIZE] = {1.0, 3.0, 3.0, 4.0};
  double b[SIZE*SIZE] = {1.0, 0.0, 0.0, 1.0};
  //double a[SIZE*SIZE] = {5.0};
  //double b[SIZE*SIZE] = {1.0};

#if 0       
  /* Inverse */
  dgesv_( &N, &NRHS, a, &LDA, IPIVOT, b, &LDB, &INFO);
          
  printf("\n INFO=%d\n", INFO );          
  printf("IPIVOT[0]=%d\n", IPIVOT[0] );          

  printf("b[%d][%d] = \n", SIZE, SIZE);
  for (i = 0;i < SIZE*SIZE; ++i)
    printf("%f ", b[i]);
  printf("\n");
  printf("a[%d][%d] = \n", SIZE, SIZE);
  for (i = 0;i < SIZE*SIZE; ++i)
    printf("%f ", a[i]);
  printf("\n");
#endif

  __CLPK_integer ipiv[3];
 
  dgetrf(&N, &N, a, &N, ipiv, &INFO);

  printf("\n INFO=%d\n", INFO );          
  printf("a[%d][%d] = \n", SIZE, SIZE);
  for (i = 0;i < SIZE*SIZE; ++i)
    printf("%f ", a[i]);
  printf("\n");

  printf("DET = %f\n", a[0]*a[3]);
}
#endif

void testMatrix()
{
  int i;
  NSAutoreleasePool *pool = [NSAutoreleasePool new];

  BCDataMatrix *m = [BCDataMatrix emptyDataMatrixWithRows: 2 andColumns: 2 andEncode: BCintEncode];
  int *a = [m dataMatrix];
  a[0] = 4;
  a[1] = 6;
  a[2] = 2;
  a[3] = 0;
  for (i = 0;i < 4; ++i)
    printf("%d ", a[i]);
  printf("\n");

  BCDataMatrix *n = [m LUfactorize];
  double *b = [n dataMatrix];
  for (i = 0;i < 4; ++i)
    printf("%lf ", b[i]);
  printf("\n");

  printf("DET = %f\n", [m determinant]);

  [pool release];
}

int
main( int argc, char** argv) 
{
  //testInverse();

  //testMatrix();

  reaction(0);
}
