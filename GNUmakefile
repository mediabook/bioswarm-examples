#
#  GNUmakefile
#  
#  Written by:	Scott Christley <schristley@mac.com>
#  Copyright (c) 2010-2014 Scott Christley
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#  1. Redistributions of source code must retain the above copyright
#  notice, this list of conditions and the following disclaimer.
#  2. Redistributions in binary form must reproduce the above copyright
#  notice, this list of conditions and the following disclaimer in the
#  documentation and/or other materials provided with the distribution.
#  3. The name of the author may not be used to endorse or promote products
#  derived from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
#  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
#  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
#  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
#  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
#  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

# Use same makefile for GNUstep and OSX
PLATFORM = $(shell uname)

#NVCCFLAGS = -w
#NVCCFLAGS = -Xptxas -v
NVCCFLAGS = -G -g

ifeq ($(PLATFORM),Darwin)

# OSX 10.9 now uses clang compiler as default
# deprecating Swarm
#ARCHFLAG = -m32
ARCHFLAG =
#LIBS = -L/usr/local/cuda/lib -lcudart -framework BioCocoa -framework BioSwarm -framework Swarm -framework Accelerate -framework Cocoa
LIBS = -L/usr/local/cuda/lib -lcudart -framework BioCocoa -framework BioSwarm -framework Accelerate -framework Cocoa

all: checkDevice gpuDynamicReaction gpuRule gpuMCMC gpuSMCSampler gpuRD gpuScEM gpu2dScEM gpuBM gpuSEM

checkDevice: checkDevice.cu.m
	cp checkDevice.cu.m checkDevice.cu
	nvcc -m64 $(NVCCFLAGS) -G -g -o checkDevice checkDevice.cu

# deprecated
#gpuReaction: gpuReaction.m
#	modelToGPU reaction.bioswarm reaction
#	gcc $(ARCHFLAG) -g -c -DMODEL_FILE=@\"reaction.bioswarm\" gpuReaction.m
#	nvcc $(ARCHFLAG) $(NVCCFLAGS) -c reaction.cu
#	g++ $(ARCHFLAG) -fPIC -g -o gpuReaction gpuReaction.o reaction.o $(LIBS)

gpuDynamicReaction: gpuDynamicReaction.m
	modelToGPU dreaction.bioswarm dreaction
	clang $(ARCHFLAG) -g -c -DMODEL_FILE=@\"dreaction.bioswarm\" gpuDynamicReaction.m
	nvcc $(ARCHFLAG) $(NVCCFLAGS) -c dreaction.cu
	clang $(ARCHFLAG) -fPIC -g -o gpuDynamicReaction gpuDynamicReaction.o dreaction.o $(LIBS)

gpuRule: gpuRule.m
	modelToGPU rule.bioswarm rule
	gcc $(ARCHFLAG) -g -c -DMODEL_FILE=@\"rule.bioswarm\" gpuRule.m
	nvcc $(ARCHFLAG) $(NVCCFLAGS) -c rule.cu
	g++ $(ARCHFLAG) -fPIC -g -o gpuRule gpuRule.o rule.o $(LIBS)

gpuRuleRKF: gpuRuleRKF.m test_rkf.cu.m test_rkf_0.cu.m test_rkf_0_defines.cu.m test_rkf_0_kernel.cu.m
	#modelToGPU rule.bioswarm rule
	cp test_rkf.cu.m test_rkf.cu
	cp test_rkf_0.cu.m test_rkf_0.cu
	cp test_rkf_0_defines.cu.m test_rkf_0_defines.cu
	cp test_rkf_0_kernel.cu.m test_rkf_0_kernel.cu
	gcc $(ARCHFLAG) -g -c -DMODEL_FILE=@\"test_rkf.bioswarm\" gpuRuleRKF.m
	nvcc $(ARCHFLAG) $(NVCCFLAGS) -c test_rkf.cu
	g++ $(ARCHFLAG) -fPIC -g -o gpuRuleRKF gpuRuleRKF.o test_rkf.o $(LIBS)

gpuMCMC: gpuMCMC.m
	modelToGPU mcmc.bioswarm mcmc
	gcc $(ARCHFLAG) -g -c -DMODEL_FILE=@\"mcmc.bioswarm\" gpuMCMC.m
	nvcc $(ARCHFLAG) $(NVCCFLAGS) -c mcmc.cu
	g++ $(ARCHFLAG) -fPIC -g -o gpuMCMC gpuMCMC.o mcmc.o $(LIBS)

gpuSMCSampler: gpuSMCSampler.m
	modelToGPU smc.bioswarm smc
	gcc $(ARCHFLAG) -g -c -DMODEL_FILE=@\"smc.bioswarm\" gpuSMCSampler.m
	nvcc $(ARCHFLAG) $(NVCCFLAGS) -c smc.cu
	g++ $(ARCHFLAG) -fPIC -g -o gpuSMCSampler gpuSMCSampler.o smc.o $(LIBS)

gpuMedium: gpuMedium.m
	modelToGPU medium.bioswarm medium
	gcc $(ARCHFLAG) -g -c -DMODEL_FILE=@\"medium.bioswarm\" gpuMedium.m
	nvcc $(ARCHFLAG) $(NVCCFLAGS) -c medium.cu
	g++ $(ARCHFLAG) -fPIC -g -o gpuMedium gpuMedium.o medium.o $(LIBS)

gpuDynamicMedium: gpuMedium.m
	modelToGPU dmedium.bioswarm dmedium
	gcc $(ARCHFLAG) -g -c -DMODEL_FILE=@\"dmedium.bioswarm\" gpuMedium.m
	nvcc $(ARCHFLAG) $(NVCCFLAGS) -c dmedium.cu
	g++ $(ARCHFLAG) -fPIC -g -o gpuDynamicMedium gpuMedium.o dmedium.o $(LIBS)

gpuRD: gpuRD.m
	modelToGPU rd.bioswarm rd
	gcc $(ARCHFLAG) -g -c -DMODEL_FILE=@\"rd.bioswarm\" gpuRD.m
	nvcc $(ARCHFLAG) $(NVCCFLAGS) -c rd.cu
	g++ $(ARCHFLAG) -fPIC -o gpuRD gpuRD.o rd.o $(LIBS)

gpuScEM: gpuScEM.m
	modelToGPU scem.bioswarm scem
	gcc -g $(ARCHFLAG) -c -DMODEL_FILE=@\"scem.bioswarm\" gpuScEM.m
	nvcc $(ARCHFLAG) $(NVCCFLAGS) -g -c scem.cu
	g++ -g $(ARCHFLAG) -fPIC -o gpuScEM gpuScEM.o scem.o $(LIBS)

gpuSEM: gpuSEM.m
	modelToGPU sem.bioswarm sem
	gcc -g $(ARCHFLAG) -c -DMODEL_FILE=@\"sem.bioswarm\" gpuSEM.m
	nvcc $(ARCHFLAG) $(NVCCFLAGS) -g -c sem.cu
	g++ -g $(ARCHFLAG) -fPIC -o gpuSEM gpuSEM.o sem.o $(LIBS)

gpu2dScEM: gpu2dScEM.m
	modelToGPU scem2d.bioswarm scem2d
	clang -g $(ARCHFLAG) -c -DMODEL_FILE=@\"scem2d.bioswarm\" gpu2dScEM.m
	nvcc $(ARCHFLAG) $(NVCCFLAGS) -g -c scem2d.cu
	clang -g $(ARCHFLAG) -fPIC -o gpu2dScEM gpu2dScEM.o scem2d.o $(LIBS)

# deprecated
#swarmGrowth: swarmGrowth.m
#	modelToGPU growth.bioswarm growth
#	gcc -g $(ARCHFLAG) -c -DMODEL_FILE=@\"growth.bioswarm\" swarmGrowth.m 
#	nvcc $(ARCHFLAG) $(NVCCFLAGS) -g -c growth.cu
#	g++ -g $(ARCHFLAG) -fPIC -o swarmGrowth swarmGrowth.o growth.o $(LIBS)

gpuBM: gpuBM.m
	modelToGPU bm.bioswarm bm
	gcc -g $(ARCHFLAG) -c -DMODEL_FILE=@\"bm.bioswarm\" gpuBM.m
	nvcc $(ARCHFLAG) $(NVCCFLAGS) -g -c bm.cu
	g++ -g $(ARCHFLAG) -fPIC -o gpuBM gpuBM.o bm.o $(LIBS)

# TODO
newSEM: newSEM.m new_growth.m new_growth_0.m new_growth_0_defines.m new_growth_0_kernel.m
	gcc -g $(ARCHFLAG) -c newSEM.m
	cp new_growth.m new_growth.cu
	cp new_growth_0.m new_growth_0.cu
	cp new_growth_0_defines.m new_growth_0_defines.cu
	cp new_growth_0_kernel.m new_growth_0_kernel.cu
	nvcc $(ARCHFLAG) $(NVCCFLAGS) -g -c new_growth.cu
	g++ -g $(ARCHFLAG) -fPIC -o newSEM newSEM.o new_growth.o $(LIBS)

clean:
	rm -f *.o
	rm -f checkDevice
	rm -f gpuReaction
	rm -f gpuDynamicReaction
	rm -f gpuRule
	rm -f gpuRuleRKF
	rm -f gpuMCMC
	rm -f gpuSMCSampler
	rm -f gpuRD
	rm -f gpuMedium gpuDynamicMedium
	rm -f gpuScEM
	rm -f gpuSEM
	rm -f gpuBM
	rm -f swarmGrowth
	rm -f newSEM

distclean: clean
	rm -f *.cu
	rm -f *~
	rm -f *.dat
	rm -f *_run_*.bioswarm

else

#
# Assume Linux and thus GNUstep
#

# This usually happens when you forget to source GNUstep.sh
ifeq ($(GNUSTEP_MAKEFILES),)
  $(error You need to run the GNUstep configuration script before compiling!)
endif

include $(GNUSTEP_MAKEFILES)/common.make

# The tools to be compiled
TOOL_NAME = \
gpuDynamicReaction \
gpuRule \
gpuMedium \
gpuDynamicMedium \
gpuMCMC \
gpuSMCSampler \
testOperation2 \
gpuRD \
gpuScEM
#testOperation \
#gpuReaction \

gpuReaction_OBJCFLAGS += -DMODEL_FILE=@\"reaction.bioswarm\"
gpuReaction_OBJC_FILES = \
gpuReaction.m \
reaction.cu.o

gpuDynamicReaction_OBJCFLAGS += -DMODEL_FILE=@\"dreaction.bioswarm\"
gpuDynamicReaction_OBJC_FILES = \
gpuDynamicReaction.m \
dreaction.cu.o

gpuRule_OBJCFLAGS += -DMODEL_FILE=@\"rule.bioswarm\"
gpuRule_OBJC_FILES = \
gpuRule.m \
rule.cu.o

gpuMedium_OBJCFLAGS += -DMODEL_FILE=@\"medium.bioswarm\"
gpuMedium_OBJC_FILES = \
gpuMedium.m \
medium.cu.o

gpuDynamicMedium_OBJCFLAGS += -DMODEL_FILE=@\"dmedium.bioswarm\"
gpuDynamicMedium_OBJC_FILES = \
gpuMedium.m \
dmedium.cu.o

gpuMCMC_OBJCFLAGS += -DMODEL_FILE=@\"mcmc.bioswarm\"
gpuMCMC_OBJC_FILES = \
gpuMCMC.m \
mcmc.cu.o

gpuSMCSampler_OBJCFLAGS += -DMODEL_FILE=@\"smc.bioswarm\"
gpuSMCSampler_OBJC_FILES = \
gpuSMCSampler.m \
smc.cu.o

gpuRD_OBJCFLAGS += -DMODEL_FILE=@\"rd.bioswarm\"
gpuRD_OBJC_FILES = \
gpuRD.m \
rd.cu.o

gpuScEM_OBJCFLAGS += -DMODEL_FILE=@\"scem.bioswarm\"
gpuScEM_OBJC_FILES = \
gpuScEM.m \
scem.cu.o

testOperation_OBJC_FILES = \
testOperation.m

testOperation2_OBJC_FILES = \
testOperation2.m

#NVCCFLAGS = -ccbin=/usr/bin/gcc-4.4

CUDA_LIBRARY_PATH = /usr/local/cuda/lib64
ADDITIONAL_LIB_DIRS += -L$(CUDA_LIBRARY_PATH)
ADDITIONAL_TOOL_LIBS += -lBioSwarm -lBioCocoa -lcudart -lstdc++

-include GNUmakefile.preamble

include $(GNUSTEP_MAKEFILES)/tool.make

-include GNUmakefile.postamble

obj/gpuReaction.obj/reaction.cu.o: reaction.bioswarm
	modelToGPU reaction.bioswarm reaction
	nvcc $(NVCCFLAGS) -c -I $(GNUSTEP_HEADERS) reaction.cu -o obj/gpuReaction.obj/reaction.cu.o

obj/gpuDynamicReaction.obj/dreaction.cu.o: dreaction.bioswarm
	modelToGPU dreaction.bioswarm dreaction
	nvcc $(NVCCFLAGS) -c -I $(GNUSTEP_HEADERS) dreaction.cu -o obj/gpuDynamicReaction.obj/dreaction.cu.o

obj/gpuRule.obj/rule.cu.o: rule.bioswarm
	modelToGPU rule.bioswarm rule
	nvcc $(NVCCFLAGS) -c -I $(GNUSTEP_HEADERS) rule.cu -o obj/gpuRule.obj/rule.cu.o

obj/gpuMedium.obj/medium.cu.o: medium.bioswarm
	modelToGPU medium.bioswarm medium
	nvcc $(NVCCFLAGS) -c -I $(GNUSTEP_HEADERS) medium.cu -o obj/gpuMedium.obj/medium.cu.o

obj/gpuDynamicMedium.obj/dmedium.cu.o: dmedium.bioswarm
	modelToGPU dmedium.bioswarm dmedium
	nvcc $(NVCCFLAGS) -c -I $(GNUSTEP_HEADERS) dmedium.cu -o obj/gpuDynamicMedium.obj/dmedium.cu.o

obj/gpuMCMC.obj/mcmc.cu.o: mcmc.bioswarm
	modelToGPU mcmc.bioswarm mcmc
	nvcc $(NVCCFLAGS) -c -I $(GNUSTEP_HEADERS) mcmc.cu -o obj/gpuMCMC.obj/mcmc.cu.o

obj/gpuSMCSampler.obj/smc.cu.o: smc.bioswarm
	modelToGPU smc.bioswarm smc
	nvcc $(NVCCFLAGS) -c -I $(GNUSTEP_HEADERS) smc.cu -o obj/gpuSMCSampler.obj/smc.cu.o

obj/gpuRD.obj/rd.cu.o: rd.bioswarm
	modelToGPU rd.bioswarm rd
	nvcc $(NVCCFLAGS) -c -I $(GNUSTEP_HEADERS) rd.cu -o obj/gpuRD.obj/rd.cu.o

obj/gpuScEM.obj/scem.cu.o: scem.bioswarm
	modelToGPU scem.bioswarm scem
	nvcc $(NVCCFLAGS) -c -I $(GNUSTEP_HEADERS) scem.cu -o obj/gpuScEM.obj/scem.cu.o

endif
