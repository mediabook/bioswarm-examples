# BioCocoa base image
FROM mediabook/bioswarm

MAINTAINER Scott Christley <schristley@mac.com>

# compile and install BioSwarm
COPY . /bioswarm-examples
RUN cd /bioswarm-examples && make
