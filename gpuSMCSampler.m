//
// gpuSMCSampler.m
//
// Sequential MCMC sampling for Basic rule-based reaction model
//

#import <Foundation/Foundation.h>
#import <BioSwarm/BioSwarmManager.h>
#import <BioSwarm/BioSwarmController.h>
#import <BioSwarm/ReactionRuleModel.h>

#import <BioCocoa/BCDataMatrix.h>
#ifdef GNUSTEP
#else
#import <Accelerate/Accelerate.h>
#endif

#define DEBUG 0

extern void *getGPUFunctionForModel(int aModel, int value);

#define OBS 12
#define VARS 3
//const int times[] = {0, 36000, 72000, 108000, 180000, 252000, 324000, 396000, 468000, 540000, 612000, 684000};
const int times[] = {0, 1800, 3600, 5400, 9000, 12600, 16200, 19800, 23400, 27000, 30600, 34200};
//const double rA[] = {0.072727, 0.057142, 0.047059, 0.040000, 0.034782, 0.030770, 0.027587, 0.025001, 0.022858, 0.021053};
//const double mA[] = {0.0726,    0.0567,    0.0481,    0.0400,    0.0346,    0.0313,    0.0283,    0.0245,    0.0226,    0.0208};
//const double mB[] = {0.0732,    0.0570,    0.0474,    0.0392,    0.0346,    0.0314,    0.0281,    0.0253,    0.0224,    0.0215};
//const double rC[] = {0.027273, 0.042857, 0.052941, 0.060000, 0.065216, 0.069227, 0.072411, 0.075007, 0.077140, 0.078957};
const double cA[] = {0.0985, 0.0637, 0.0500, 0.0462, 0.0363, 0.0248, 0.0171, 0.0168, 0.0131, 0.0150, 0.0140, 0.0134};
const double cB[] = {0.0995, 0.0651, 0.0596, 0.0453, 0.0384, 0.0247, 0.0174, 0.0203, 0.0136, 0.0121, 0.0142, 0.0134};
const double cC[] = {0.0010, 0.0357, 0.0501, 0.0512, 0.0682, 0.0747, 0.0809, 0.0818, 0.0858, 0.0863, 0.0872, 0.0928};

@interface MyDelegate : NSObject
{
  int totalParameters;
  NSArray *parameterNames;
  NSArray *parameterIndexes;
  NSArray *parameterModelNumbers;
  NSArray *parameterModelKeys;
  double *parameterMinRange;
  double *parameterMaxRange;
  void *acceptParametersValues;
  void *rejectParametersValues;

#if 0
  int numOfParticles;
  BCDataMatrix *Y;
  NSMutableArray *Y_hat;
  BCDataMatrix *Delta;
  BCDataMatrix *S;
#endif
}
- (void)didTransferDataToGPU: (BOOL)aFlag sender: (id)theController;

// MCMC delegate
- (void)parameterInfo: theController names: (NSArray *)names indexes: (NSArray *)indexes
	       modelNumbers: (NSArray *)modelNumbers modelKeys: (NSArray *)modelKeys
            minRanges: (double *)minRange maxRange: (double *)maxRange
         acceptValues: (void *)acceptValues rejectValues:(void *)rejectValues;
- (void)willCalculatePosteriorDensity: theController forIndividual: (int)modelIndex;
- (double)calculatePosteriorDensity: theController forIndividual: (int)modelIndex;
- (void)initExpectation: theController;
- (void)calculateExpectation: theController weights: (double *)particleWeights accept: (BOOL *)acceptFlags;
- (void)finishExpectation: theController;

@end

@implementation MyDelegate
- (void)didTransferDataToGPU: (BOOL)aFlag sender: (id)theController
{
}

//
// MCMC delegate
//

- (void)parameterInfo: theController names: (NSArray *)names indexes: (NSArray *)indexes
	       modelNumbers: (NSArray *)modelNumbers modelKeys: (NSArray *)modelKeys
            minRanges: (double *)minRange maxRange: (double *)maxRange
         acceptValues: (void *)acceptValues rejectValues:(void *)rejectValues
{
  parameterNames = names;
  parameterIndexes = indexes;
  parameterModelNumbers = modelNumbers;
  parameterModelKeys = modelKeys;
  parameterMinRange = minRange;
  parameterMaxRange = maxRange;
  totalParameters = [parameterNames count];
  acceptParametersValues = acceptValues;
  rejectParametersValues = rejectValues;
  printf("parameterInfo: %d\n", totalParameters);
}

#if 0
- (void)allocatePosterior: theController
{
  int i;

  if (!Y_hat) {
    // prediction data
    numOfParticles = [[theController modelInstances] count];

    Y = [[BCDataMatrix emptyDataMatrixWithRows: OBS andColumns: VARS andEncode: BCdoubleEncode] retain];
    Delta = [[BCDataMatrix emptyDataMatrixWithRows: OBS andColumns: VARS andEncode: BCdoubleEncode] retain];
    S = [[BCDataMatrix emptyDataMatrixWithRows: VARS andColumns: VARS andEncode: BCdoubleEncode] retain];

    Y_hat = [NSMutableArray new];
    for (i = 0; i < numOfParticles; ++i) {
      [Y_hat addObject: [BCDataMatrix emptyDataMatrixWithRows: OBS andColumns: VARS andEncode: BCdoubleEncode]];
    }

    double (*y)[OBS][VARS] = [Y dataMatrix];
    for (i = 0; i < OBS; ++i) {
      (*y)[i][0] = cA[i];
      (*y)[i][1] = cB[i];
      (*y)[i][2] = cC[i];
    }
  }
}

- (void)willCalculatePosteriorDensity: theController forIndividual: (int)modelIndex
{
  int i;

  if (!S) {
    if (DEBUG) printf("willCalculatePosteriorDensity:forIndividual:\n");

    [self allocatePosterior: theController];

    // zero output file
    FILE *out = fopen("smc_exp.txt", "w");
    for (i = 0; i < totalParameters; ++i) {
      if (i != 0) fprintf(out, " ");
      NSString *pn = [parameterNames objectAtIndex: i];
      fprintf(out, "%s", [pn UTF8String]);
    }
    fprintf(out, " weight\n");
    fclose(out);
  }
}

- (double)calculatePosteriorDensity: theController forIndividual: (int)modelIndex
{
  if (DEBUG) printf("calculatePosteriorDensity:forIndividual:\n");

  int i, j, k;
  double (*y)[OBS][VARS] = [Y dataMatrix];
  double (*d)[OBS][VARS] = [Delta dataMatrix];
  double (*s)[VARS][VARS] = [S dataMatrix];

  double (*yhat)[OBS][VARS];
  yhat = [[Y_hat objectAtIndex: modelIndex] dataMatrix];

  if (DEBUG) for (i = 0; i < OBS; ++i) printf("(%d) %d: %f %f %f\n", modelIndex, i, (*yhat)[i][0], (*yhat)[i][1], (*yhat)[i][2]);

  // Delta = Y - Y_hat
  for (i = 0; i < OBS; ++i) {
    (*d)[i][0] = (*y)[i][0] - (*yhat)[i][0];
    (*d)[i][1] = (*y)[i][1] - (*yhat)[i][1];
    (*d)[i][2] = (*y)[i][2] - (*yhat)[i][2];
  }

  // S = Delta' * Delta
  for (i = 0; i < 3; ++i)
    for (j = 0; j < 3; ++j) {
      (*s)[i][j] = 0.0;
      for (k = 0; k < OBS; ++k)
	(*s)[i][j] += (*d)[k][i] * (*d)[k][j];
    }

  if (DEBUG) {
    for (i = 0; i < 3; ++i) {
      for (j = 0; j < 3; ++j)
	printf("%e ", (*s)[i][j]);
      printf("\n");
    }
  }

  double det = fabs([S determinant]);
  if (DEBUG) printf("det(S) = %e\n", det);
  double pd = pow(det, - ((double)OBS) / 2.0);
  if (DEBUG) printf("pd = %e\n", pd);
  return pd;
}
#endif

- (void)initExpectation: theController
{
  int numOfParticles = [[theController modelInstances] count];
  printf("initExpectation:  %d %d\n", numOfParticles, totalParameters);
}

- (void)calculateExpectation: theController weights: (double *)particleWeights accept: (BOOL *)acceptFlags
{
  int i, j;
  if (DEBUG) printf("calculateExpectation:weights:accept:\n");
  int numOfParticles = [[theController modelInstances] count];
  printf("calculateExpectation:weights:accept:  %d %d\n", numOfParticles, totalParameters);

  double (*acceptParameters)[numOfParticles][totalParameters] = acceptParametersValues;
  double (*rejectParameters)[numOfParticles][totalParameters] = rejectParametersValues;

  FILE *out = fopen("smc_exp.txt", "a");
  for (i = 0; i < numOfParticles; ++i) {
    for (j = 0; j < totalParameters; ++j) {
      if (j != 0) fprintf(out, " ");
      if (acceptFlags[i]) fprintf(out, "%f", (*acceptParameters)[i][j]);
      else fprintf(out, "%f", (*rejectParameters)[i][j]);
    }
    fprintf(out, " %e\n", particleWeights[i]);
  }
  fclose(out);
}

- (void)finishExpectation: theController
{
}

@end

int reaction(int my_rank)
{
  NSMutableDictionary *runParameters;
  NSAutoreleasePool *pool;
  NSString *paramFile;
  id s;

  pool = [NSAutoreleasePool new];

  BioSwarmManager *simManager = [[BioSwarmManager alloc] initWithDefaultModelFile: MODEL_FILE];
  id delegate = [MyDelegate new];
  [simManager setDelegate: delegate];

  if ([simManager processArguments: [[NSProcessInfo processInfo] arguments]]) {
    [simManager setGPUFunctions: &getGPUFunctionForModel];
    [simManager startSimulation];
  }

  [simManager release];
  [delegate release];

  [pool release];
  return 0;
}

int
main( int argc, char** argv) 
{
  return reaction(0);
}
