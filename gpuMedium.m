//
// gpuMedium.m
//
// a medium-sized reaction rule model for testing
//

#include <Foundation/Foundation.h>
#import <BioSwarm/SimulationController.h>
#import <BioSwarm/ReactionRuleModel.h>
#import <BioCocoa/BCExpressionData.h>
#import <BioSwarm/DataHandler.h>
#import <BioSwarm/BioSwarmManager.h>

extern void *getGPUFunctionForModel(int aModel, int value);

#define BINS 100
#define DEBUG 1

@interface MyDelegate : NSObject
{
  float **prevValues;
  int *timeCount;
  BCDataMatrix *exprData;
  int numOfObservations;
  int numOfVariables;
  //NSMutableArray *ratioObservations;
  BCDataMatrix *ratioMatrix;
  int *speciesIndex;
  BCDataMatrix *fitnessMatrix;
  int numOfPredictions;

  NSArray *speciesNames;
  NSArray *speciesIndexes;
  NSArray *speciesModelNumbers;
  NSArray *speciesModelKeys;

  int totalParameters;
  NSArray *parameterNames;
  NSArray *parameterIndexes;
  NSArray *parameterModels;
  double *parameterMinRange;
  double *parameterMaxRange;
  void *acceptParametersValues;
  void *rejectParametersValues;

  int numOfParticles;
  BCDataMatrix *Y;
  NSMutableArray *Y_hat;
  BCDataMatrix *Delta;
  BCDataMatrix *S;
  NSString *expectationFileName;

  double N_below;
  double N_above;
  double hist[BINS];
}

- (void)didInitializeSimulation: (id)theController;
- (void)didTransferDataToGPU: (BOOL)aFlag sender: (SimulationController *)theController;
- (void)didFinishSimulation: (id)theController;

- (void)speciesInfo: theController names: (NSArray *)names indexes: (NSArray *)indexes modelNumbers: (NSArray *)modelNumbers
	  modelKeys: (NSArray *)modelKeys;


// Evolutionary strategy
- (double)calculateFitness: theController forIndividual: (int)modelIndex;
- (void)bestFitness: (double)aFitness forGeneration: (int)aGen controller: theController individual: (int)modelIndex;

// MCMC delegate
- (void)parameterInfo: theController names: (NSArray *)names indexes: (NSArray *)indexes models: (NSArray *)models
            minRanges: (double *)minRange maxRange: (double *)maxRange acceptValues: (void *)acceptValues rejectValues: (void *)rejectValues;
- (void)willPerformMCMC: theController setNumber: (int)setNum;
- (void)didPerformMCMC: theController setNumber: (int)setNum;
- (void)allocatePosterior: theController;
- (void)willCalculatePosteriorDensity: theController forIndividual: (int)modelIndex;
- (double)calculatePosteriorDensity: theController forIndividual: (int)modelIndex;
- (void)initExpectation: theController;
- (void)calculateExpectation: theController weights: (double *)particleWeights accept: (BOOL *)acceptFlags;
- (void)finishExpectation: theController;
@end

@implementation MyDelegate
- init
{
  int j, k;

  [super init];

  exprData = [BCDataMatrix dataMatrixWithContentsOfFile: @"expressionData.txt"
			   andEncode: BCdoubleEncode
			   andFormat: [NSDictionary dictionaryWithObjectsAndKeys:
						      [NSNumber numberWithBool: YES], BCParseColumnNames, nil]];
  [exprData retain];
  numOfObservations = [exprData numberOfRows];
  numOfVariables = [exprData numberOfColumns];
  double (*EM)[numOfObservations][numOfVariables];
  EM = [exprData dataMatrix];
  ratioMatrix = [BCDataMatrix emptyDataMatrixWithRows: numOfObservations andColumns: numOfVariables andEncode: BCdoubleEncode];
  double (*DM)[numOfVariables][numOfVariables];
  DM = [ratioMatrix dataMatrix];

  for (k = 0; k < numOfObservations; ++k) {
    numOfPredictions = 0;
    for (j = 1; j < numOfVariables; ++j) {
      (*DM)[k][j] = (*EM)[k][j] / (*EM)[k][0];
      ++numOfPredictions;
    }
  }
#if DEBUG
  print_matrix("ratio = \n", numOfObservations, numOfVariables, DM);
  printf("numOfObservations = %d\n", numOfObservations);
  printf("numOfVariables = %d\n", numOfVariables);
  printf("numOfPredictions = %d\n", numOfPredictions);
#endif

  speciesIndex = NULL;
  fitnessMatrix = [BCDataMatrix emptyDataMatrixWithRows: numOfVariables andColumns: numOfVariables andEncode: BCfloatEncode];
  [fitnessMatrix retain];

  return self;
}
- (void)dealloc
{
  [exprData release];
  //[ratioObservations release];
  [fitnessMatrix release];
  [Y_hat release];
  [Y release];
  [Delta release];
  [S release];

  [super dealloc];
}

- (void)didInitializeSimulation: (id)theController
{
#if 0
  int i, modelIndex;

  int numModels = [theController numModelInstances];

  timeCount = (int *)malloc(sizeof(int) * numModels);
  prevValues = (float **)malloc(sizeof(float *) * numModels);

  for (modelIndex = 0; modelIndex < numModels; ++modelIndex) {
    ReactionRuleModel *rm = [theController getModel: modelIndex withName: @"PseudomonasQS"];
    int numOfSpecies = [rm numOfSpecies];
    float (*data)[numOfSpecies] = [rm dataResults];
    prevValues[modelIndex] = (float *)malloc(sizeof(float) * numOfSpecies);

    for (i = 0; i < numOfSpecies; ++i) {
      prevValues[modelIndex][i] = (*data)[i];
    }
    timeCount[modelIndex] = 0;
  }
#endif
}

- (void)didTransferDataToGPU: (BOOL)aFlag sender: (SimulationController *)theController
{
#if 1
#if DEBUG
  int i, modelIndex;
  NSArray *modelInstances = [theController modelInstances];
  for (modelIndex = 0; modelIndex < [modelInstances count]; ++modelIndex) {
    id modelObject = [modelInstances objectAtIndex: modelIndex];
    for (i = 0; i < [speciesNames count]; ++i) {
      int mnum = [[speciesModelNumbers objectAtIndex: i] intValue];
      int sidx = [[speciesIndexes objectAtIndex: i] intValue];
      id m = [modelObject modelWithNumber: mnum];
      int numOfSpecies = [m numOfSpecies];
      float (*data)[numOfSpecies] = [m dataResults];
      
      printf("instance (%d) species: %s = %f\n", modelIndex, [[speciesNames objectAtIndex: i] UTF8String], (*data)[sidx]);
    }
  }
#endif
#else
  int i, j, modelIndex;
  float epsilon = 0.001;

  int numModels = [theController numModelInstances];
  //printf("# models = %d\n", numModels);
  for (modelIndex = 0; modelIndex < numModels; ++modelIndex) {
    ReactionRuleModel *rm = [theController getModel: modelIndex withName: @"PseudomonasQS"];
    int numOfSpecies = [rm numOfSpecies];
    float (*data)[numOfSpecies] = [rm dataResults];

    BOOL done = YES;
    for (i = 0; i < numOfSpecies; ++i) {
      if (numModels == 1) {
	//if (DEBUG) printf("model (%d): %s = %f\n", modelIndex, [[rm nameOfSpecies: i] UTF8String], (*data)[i]);
	if ([theController currentTime] > 0)
	  if (DEBUG) printf("model (%d): %s = %f %f\n", modelIndex, [[rm nameOfSpecies: i] UTF8String], (*data)[i],
			    (*data)[i]/(*data)[speciesIndex[0]]);
      }

      // check if value is at steady state
      if (fabs((*data)[i] - prevValues[modelIndex][i]) >= epsilon) done = NO;
      prevValues[modelIndex][i] = (*data)[i];
    }

    if (numModels == 1) {
      [self allocatePosterior: theController];

      double (*yhat)[1][numOfVariables];
      yhat = [[Y_hat objectAtIndex: modelIndex] dataMatrix];

      for (j = 1; j < numOfVariables; ++j) {
	(*yhat)[0][j] = (*data)[speciesIndex[j]] / (*data)[speciesIndex[0]];
      }

      [self calculatePosteriorDensity: theController forIndividual: 0];
    }

    if (!done) ++timeCount[modelIndex];
  }
#endif
}

- (void)didFinishSimulation: (id)theController
{
#if 0
  int i, j, modelIndex;

  int numModels = [theController numModelInstances];
  if (DEBUG) printf("# models = %d\n", numModels);

  if (numModels > 1) {
    FILE *final = fopen("PseudomonasQS_final.dat", "w");

    for (modelIndex = 1; modelIndex < numModels; ++modelIndex) {
      ReactionRuleModel *rm = [theController getModel: modelIndex withName: @"PseudomonasQS"];
      int numOfSpecies = [rm numOfSpecies];
      float (*data)[numOfSpecies] = [rm dataResults];

      for (i = 0; i < numOfSpecies; ++i) {
	fprintf(final, "%f ", (*data)[i]);
      }
      fprintf(final, " %d\n", timeCount[modelIndex]);
    }
    fclose(final);
  } else {
    ReactionRuleModel *rm = [theController getModel: 0 withName: @"PseudomonasQS"];
    int numOfSpecies = [rm numOfSpecies];
    float (*data)[numOfSpecies] = [rm dataResults];

    if (DEBUG)
      for (i = 0; i < numOfSpecies; ++i) {
	printf("model (0): %s = %f\n", [[rm nameOfSpecies: i] UTF8String], (*data)[i]);
      }
  }

  // save predictions for MCMC
  if (Y_hat) {
    for (modelIndex = 0; modelIndex < numModels; ++modelIndex) {
      ReactionRuleModel *rm = [theController getModel: modelIndex withName: @"PseudomonasQS"];
      int numOfSpecies = [rm numOfSpecies];
      float (*data)[numOfSpecies] = [rm dataResults];

#if 1
      double (*yhat)[1][numOfVariables];
      yhat = [[Y_hat objectAtIndex: modelIndex] dataMatrix];

      for (j = 1; j < numOfVariables; ++j) {
	(*yhat)[0][j] = (*data)[speciesIndex[j]] / (*data)[speciesIndex[0]];
      }
#if DEBUG
      printf("(%d) ", modelIndex);
      print_matrix("individual = \n", 1, numOfVariables, yhat);
#endif
#else
      double (*yhat)[numOfObservations][numOfVariables];
      yhat = [[Y_hat objectAtIndex: modelIndex] dataMatrix];

      for (i = 0; i < numOfVariables; ++i)
	(*yhat)[0][i] = (*data)[speciesIndex[i]];
      printf("(%d) ", modelIndex);
      print_matrix("individual = \n", numOfObservations, numOfVariables, yhat);
#endif
    }
  }
#endif
}

- (void)cacheSpeciesIndex: (ReactionRuleModel *)rm
{
  int i;

  if (speciesIndex) return;

  // cache the index for each species
  int numOfSpecies = [rm numOfSpecies];
  NSArray *colNames = [exprData columnNames];
  speciesIndex = (int *)malloc(numOfVariables * sizeof(int));
  for (i = 0; i < numOfSpecies; ++i) {
    NSString *s = [rm nameOfSpecies: i];
    NSUInteger k = [colNames indexOfObject: s];
    if (k != NSNotFound) speciesIndex[k] = i;
  }
#if DEBUG
  for (i = 0; i < numOfVariables; ++i)
    printf("index: (%s) %d -> %d\n", [[colNames objectAtIndex: i] UTF8String], i, speciesIndex[i]);
#endif
}

- (void)speciesInfo: theController names: (NSArray *)names indexes: (NSArray *)indexes modelNumbers: (NSArray *)modelNumbers
	  modelKeys: (NSArray *)modelKeys
{
  //printf("speciesInfo\n");

  speciesNames = names;
  speciesIndexes = indexes;
  speciesModelNumbers = modelNumbers;
  speciesModelKeys = modelKeys;

  int i;
#if DEBUG
  for (i = 0; i < [speciesNames count]; ++i) {
    printf("species: %s (%d) model: %s (%d)\n", [[speciesNames objectAtIndex: i] UTF8String],
           [[speciesIndexes objectAtIndex: i] intValue], [[speciesModelKeys objectAtIndex: i] UTF8String],
           [[speciesModelNumbers objectAtIndex: i] intValue]);
  }
#endif

  // cache the index for each species
  NSArray *colNames = [exprData columnNames];
  speciesIndex = (int *)malloc(numOfVariables * sizeof(int));
  for (i = 0; i < [speciesNames count]; ++i) {
    NSString *s = [speciesNames objectAtIndex: i];
    NSUInteger k = [colNames indexOfObject: s];
    if (k != NSNotFound) speciesIndex[k] = i;
  }
#if DEBUG
  for (i = 0; i < numOfVariables; ++i)
    printf("index: (%s) %d -> %d\n", [[colNames objectAtIndex: i] UTF8String], i, speciesIndex[i]);
#endif
}

- (double)calculateFitness: theController forIndividual: (int)modelIndex
{
#if 1
  NSArray *modelInstances = [theController modelInstances];
  id modelObject = [modelInstances objectAtIndex: modelIndex];

  int i, k;
  double fitness = 0.0;
  double (*DM)[numOfObservations][numOfVariables];
  DM = [ratioMatrix dataMatrix];

  for (i = 1; i < numOfVariables; ++i) {
    int mnum = [[speciesModelNumbers objectAtIndex: speciesIndex[i]] intValue];
    int sidx = [[speciesIndexes objectAtIndex: speciesIndex[i]] intValue];
    int s0idx = [[speciesIndexes objectAtIndex: speciesIndex[0]] intValue];
    id m = [modelObject modelWithNumber: mnum];
    int numOfSpecies = [m numOfSpecies];
    float (*data)[numOfSpecies] = [m dataResults];

    for (k = 0; k < numOfObservations; ++k) {
      double d = (*data)[sidx] / (*data)[s0idx];
      d -= (*DM)[k][i];
      fitness += d * d;
    }

    printf("fitness(%d, %p) %s %d = %f, %f\n", modelIndex, modelObject,
	   [[speciesNames objectAtIndex: speciesIndex[i]] UTF8String], sidx, (*data)[sidx], fitness);

  }

  return fitness;
#else
  int k, j;
  //int numModels = [theController numModelInstances];
  ReactionRuleModel *rm = [theController getModel: modelIndex withName: @"PseudomonasQS"];
  int numOfSpecies = [rm numOfSpecies];
  float (*data)[numOfSpecies] = [rm dataResults];

  [self cacheSpeciesIndex: rm];

  double fitness = 0.0;

  double (*DM)[numOfObservations][numOfVariables];
  DM = [ratioMatrix dataMatrix];
  //double (*FM)[numOfVariables][numOfVariables];
  //FM = [fitnessMatrix dataMatrix];

  for (k = 0; k < numOfObservations; ++k) {
    for (j = 1; j < numOfVariables; ++j) {
      double d = (*data)[speciesIndex[j]] / (*data)[speciesIndex[0]];
      d -= (*DM)[k][j];
      fitness += d * d;
    }
  }

  return fitness;
#endif
}
- (void)bestFitness: (double)aFitness forGeneration: (int)aGen controller: theController individual: (int)modelIndex
{
  //ReactionRuleModel *rm = [theController getModel: modelIndex withName: @"PseudomonasQS"];
  //int numOfSpecies = [rm numOfSpecies];
  //float (*data)[numOfSpecies] = [rm dataResults];

  printf("New best individual: (%d)%f at generation %d\n", modelIndex, aFitness, aGen);
  int i;
  NSArray *modelInstances = [theController modelInstances];
  id modelObject = [modelInstances objectAtIndex: modelIndex];
  for (i = 0; i < [speciesNames count]; ++i) {
    int mnum = [[speciesModelNumbers objectAtIndex: i] intValue];
    int sidx = [[speciesIndexes objectAtIndex: i] intValue];
    id m = [modelObject modelWithNumber: mnum];
    int numOfSpecies = [m numOfSpecies];
    float (*data)[numOfSpecies] = [m dataResults];
      
    printf("instance (%d) species: %s = %f\n", modelIndex, [[speciesNames objectAtIndex: i] UTF8String], (*data)[sidx]);
  }
  //[theController saveSpecificationForModel: modelIndex forRun: (aGen+1)];
}


//
// MCMC delegate
//
- (void)parameterInfo: theController names: (NSArray *)names indexes: (NSArray *)indexes models: (NSArray *)models
            minRanges: (double *)minRange maxRange: (double *)maxRange acceptValues: (void *)acceptValues rejectValues:(void *)rejectValues
{
  parameterNames = names;
  parameterIndexes = indexes;
  parameterModels = models;
  parameterMinRange = minRange;
  parameterMaxRange = maxRange;
  totalParameters = [parameterNames count];
  acceptParametersValues = acceptValues;
  rejectParametersValues = rejectValues;
  if (DEBUG) printf("parameterInfo: %d\n", totalParameters);
}

- (void)willPerformMCMC: theController setNumber: (int)setNum
{
  printf("willPerformMCMC:setNumber:\n");
  int i;

  expectationFileName = [[NSString stringWithFormat: @"smc_exp_%d.dat", setNum] retain];

  // zero output file
  FILE *out = fopen([expectationFileName UTF8String], "w");
  for (i = 0; i < totalParameters; ++i) {
    if (i != 0) fprintf(out, " ");
    NSString *pn = [parameterNames objectAtIndex: i];
    fprintf(out, "%s", [pn UTF8String]);
  }
  fprintf(out, " weight\n");
  fclose(out);
}

- (void)didPerformMCMC: theController setNumber: (int)setNum
{
  [expectationFileName release];
  expectationFileName = nil;
}

- (void)allocatePosterior: theController
{
  int i;

  if (!Y_hat) {
    ReactionRuleModel *rm = [theController getModel: 0 withName: @"PseudomonasQS"];
    [self cacheSpeciesIndex: rm];

    // prediction data
    numOfParticles = [[theController modelInstances] count];
    Y_hat = [NSMutableArray new];
    Delta = [BCDataMatrix emptyDataMatrixWithRows: numOfObservations andColumns: numOfPredictions andEncode: BCdoubleEncode];
    [Delta retain];
    S = [BCDataMatrix emptyDataMatrixWithRows: numOfPredictions andColumns: numOfPredictions andEncode: BCdoubleEncode];
    [S retain];
    for (i = 0; i < numOfParticles; ++i) {
      [Y_hat addObject: [BCDataMatrix emptyDataMatrixWithRows: 1 andColumns: numOfVariables andEncode: BCdoubleEncode]];
    }
  }
}

- (void)willCalculatePosteriorDensity: theController forIndividual: (int)modelIndex
{
#if 0
  //int i;

  // allocate data structures
  if (!Y_hat) {
    if (DEBUG) printf("willCalculatePosteriorDensity:forIndividual:\n");
    
    [self allocatePosterior: theController];

  }
#endif
}

- (double)calculatePosteriorDensity: theController forIndividual: (int)modelIndex
{
  if (DEBUG) printf("calculatePosteriorDensity:forIndividual:\n");

  int i, j, k;
  //double (*y)[numOfObservations][numOfVariables] = [exprData dataMatrix];
  //double (*yhat)[numOfObservations][numOfVariables] = [[Y_hat objectAtIndex: modelIndex] dataMatrix];
  double (*d)[numOfObservations][numOfPredictions] = [Delta dataMatrix];
  double (*s)[numOfPredictions][numOfPredictions] = [S dataMatrix];

  //for (i = 0; i < OBS; ++i) printf("%d: %f %f %f\n", i, (*yhat)[i][0], (*yhat)[i][1], (*yhat)[i][2]);

  double (*y)[numOfObservations][numOfVariables] = [ratioMatrix dataMatrix];
  double (*yhat)[1][numOfVariables] = [[Y_hat objectAtIndex: modelIndex] dataMatrix];

#if 1
  // Delta = Y - Y_hat
  double totError = 0.0;
  for (k = 0; k < numOfObservations; ++k) {
    int p = 0;
    for (j = 1; j < numOfVariables; ++j) {
      (*d)[k][p] = (*y)[k][j] - (*yhat)[0][j];
      //(*d)[k][p] = ((*y)[k][j] - (*yhat)[0][j]) / (*y)[k][j];
      totError += (*d)[k][p] * (*d)[k][p];
      ++p;
    }
  }
#else
  // Delta = Y - Y_hat
  for (i = 0; i < numOfVariables; ++i)
    (*d)[0][i] = (*y)[0][i] - (*yhat)[0][i];    
#endif

#if DEBUG
  printf("d = \n");
  for (k = 0; k < numOfObservations; ++k) {
    for (i = 0; i < numOfPredictions; ++i) printf("%f ", (*d)[k][i]);
    printf("\n");
  }
#endif

  // S = Delta' * Delta
  for (i = 0; i < numOfPredictions; ++i)
    for (j = 0; j < numOfPredictions; ++j) {
      (*s)[i][j] = 0.0;
      for (k = 0; k < numOfObservations; ++k)
	(*s)[i][j] += (*d)[k][i] * (*d)[k][j];
    }

#if DEBUG
  printf("S = \n");
  for (i = 0; i < numOfPredictions; ++i) {
    for (j = 0; j < numOfPredictions; ++j)
      printf("%e ", (*s)[i][j]);
    printf("\n");
  }
#endif

  //double det = fabs([S determinant]);
  //printf("det(S) = %e\n", det);
  //double pd = pow(det, - ((double)numOfPredictions) / 2.0);
  double pd = pow(totError, - ((double)numOfPredictions) / 2.0);

#if DEBUG
  printf("totError = %e\n", totError);
  printf("(%d) pd = %e\n", modelIndex, pd);
#endif

  return pd;
}

- (void)initExpectation: theController
{
  int i;
  for (i = 0; i < BINS; ++i) hist[i] = 0.0;
}

- (void)calculateExpectation: theController weights: (double *)particleWeights accept: (BOOL *)acceptFlags
{
  int i, j;
  if (DEBUG) printf("calculateExpectation:weights:accept\n");

  double (*acceptParameters)[numOfParticles][totalParameters] = acceptParametersValues;
  double (*rejectParameters)[numOfParticles][totalParameters] = rejectParametersValues;

  FILE *out = fopen([expectationFileName UTF8String], "a");
  for (i = 0; i < numOfParticles; ++i) {
    for (j = 0; j < totalParameters; ++j) {
      if (j != 0) fprintf(out, " ");
      if (acceptFlags[i]) fprintf(out, "%f", (*acceptParameters)[i][j]);
      else fprintf(out, "%f", (*rejectParameters)[i][j]);
    }
    fprintf(out, " %f\n", particleWeights[i]);
  }
  fclose(out);
}

- (void)finishExpectation: theController
{
}

@end

//
// Standard simulation
//
int pseudomonas()
{
  NSAutoreleasePool *pool;

  pool = [NSAutoreleasePool new];

  SimulationController *theSim = [[SimulationController alloc] initWithDefaultModelFile: MODEL_FILE];
  id delegate = [MyDelegate new];
  [theSim setDelegate: delegate];

  if ([theSim processArguments: [[NSProcessInfo processInfo] arguments]]) {
    [theSim saveModelSpecification];
    [theSim setGPUFunctions: &getGPUFunctionForModel];
    [theSim startSimulation];
  }

  [theSim release];
  [delegate release];

  [pool release];
  return 0;
}

int pseudomonas_new()
{
  NSAutoreleasePool *pool;

  pool = [NSAutoreleasePool new];

  BioSwarmManager *simManager = [[BioSwarmManager alloc] initWithDefaultModelFile: MODEL_FILE];
  id delegate = [MyDelegate new];
  [simManager setDelegate: delegate];

  if ([simManager processArguments: [[NSProcessInfo processInfo] arguments]]) {
    [simManager setGPUFunctions: &getGPUFunctionForModel];
    [simManager startSimulation];
  }

  [simManager release];
  [delegate release];

  [pool release];
  return 0;
}

//
// Collect expression data
//
void expressionData()
{
  NSAutoreleasePool *pool = [NSAutoreleasePool new];

  NSDictionary *modelDesc = [NSDictionary dictionaryWithContentsOfFile: MODEL_FILE];
  NSDictionary *exprDesc = [modelDesc objectForKey: @"expressionData"];
  NSDictionary *pData = [exprDesc objectForKey: @"pseudomonas"];

  NSString *geFile = [pData objectForKey: @"expressionFile"];
  NSString *platformTag = [pData objectForKey: @"platformTag"];
  NSDictionary *speciesMap = [pData objectForKey: @"speciesMap"];
  NSMutableArray *sampleList = [pData objectForKey: @"samples"];
  
  BCExpressionData *geData = [[BCExpressionData alloc] initWithExpressionFile: geFile];
  [geData setIgnoreMissingGenes: YES];

  NSArray *speciesList = [speciesMap allKeys];
  NSArray *geneList = [speciesMap objectsForKeys: speciesList notFoundMarker: [NSNull null]];

  NSArray *probesUsed;
  BCDataMatrix *geneMatrix = [geData extractDatasetForGenes:geneList fromSamples:sampleList probesUsed:&probesUsed platformTag: platformTag];
  [geneMatrix setColumnNames: speciesList];

  int numOfObservations = [geneMatrix numberOfRows];
  int numOfVariables = [geneMatrix numberOfColumns];
  NSArray *columnNames = [geneMatrix columnNames];
  //double (*DM)[numOfObservations][numOfVariables];
  //DM = [geneMatrix dataMatrix];
  printf("%d observations\n", numOfObservations);
  printf("%d variables\n", numOfVariables);
  printf("%s\n", [[columnNames description] UTF8String]);

  [geneMatrix writeToFile: @"expressionData.txt"];

  [pool release];
}

int main(int argc, const char *argv[])
{
  //sleep(10);

  //pseudomonas();
  pseudomonas_new();

  //expressionData();

  return 0;
}
