//
// gpuSEM.m
//

#include <Foundation/Foundation.h>
#import <BioSwarm/BioSwarmManager.h>
#import <BioSwarm/SubcellularElementModel.h>
#import <BioSwarm/CellColonyModel.h>

extern void *getGPUFunctionForModel(int, int);

@interface MyDelegate : NSObject
{
}
- (void)didTransferDataToGPU: (BOOL)aFlag sender: (id)theController;
@end
@implementation MyDelegate
- (void)didTransferDataToGPU: (BOOL)aFlag sender: (id)theController
{
#if 0
  printf("didTransferDataToGPU:\n");
  if (!aFlag) {
    CellColonyModel *colony = [theController getModel: 0 withName: @"BasicCell"];
    SubcellularElementModel *sem = [colony cellSpatialModel];
    int numOfCells = [sem numOfCells];
    int maxCells = [sem maxCells];
    int maxElements = [sem maxElements];
    int *numOfElements = [sem numOfElements];
    float (*X)[maxElements][maxCells] = [sem elementX];
    float (*Y)[maxElements][maxCells] = [sem elementY];
    float (*Z)[maxElements][maxCells] = [sem elementZ];
    int i, j;

    printf("%d %d %d\n", numOfCells, maxCells, maxElements);

    for (i = 0; i < numOfCells; ++i) {
      for (j = 0; j < numOfElements[i]; ++j) {
#if 1
	if (isnan((*X)[j][i]) || isnan((*Y)[j][i]) || isnan((*Z)[j][i])
	    || ([theController currentTime] > 3400))
#endif
	  printf("(%d, %d): (%f, %f, %f)\n", i, j,
		 (*X)[j][i], (*Y)[j][i], (*Z)[j][i]);
      }
    }
  }
#endif
}
@end

int sem(int my_rank)
{
  NSMutableDictionary *runParameters;
  NSAutoreleasePool *pool;
  NSString *paramFile;
  id s;

  pool = [NSAutoreleasePool new];

  BioSwarmManager *simManager = [[BioSwarmManager alloc] initWithDefaultModelFile: MODEL_FILE];
  id delegate = [MyDelegate new];
  [simManager setDelegate: delegate];

  if ([simManager processArguments: [[NSProcessInfo processInfo] arguments]]) {
    [simManager setGPUFunctions: &getGPUFunctionForModel];
    [simManager startSimulation];
  }

  [simManager release];
  [delegate release];

  [pool release];
  return 0;
}

int
main( int argc, char** argv) 
{
  sem(0);
}
