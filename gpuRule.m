//
// gpuRule.m
//
// Basic rule-based reaction model
//

#import <Foundation/Foundation.h>
#import <BioSwarm/ReactionRuleModel.h>
#import <BioSwarm/BioSwarmManager.h>

extern void *getGPUFunctionForModel(int aModel, int value);

@interface MyDelegate : NSObject
{
  NSArray *speciesNames;
  NSArray *speciesIndexes;
  NSArray *speciesModelNumbers;
  NSArray *speciesModelKeys;
}

- (void)speciesInfo: theController names: (NSArray *)names indexes: (NSArray *)indexes modelNumbers: (NSArray *)modelNumbers
	  modelKeys: (NSArray *)modelKeys;

- (void)didTransferDataToGPU: (BOOL)aFlag sender: (id)theController;
@end

@implementation MyDelegate

- (void)speciesInfo: theController names: (NSArray *)names indexes: (NSArray *)indexes modelNumbers: (NSArray *)modelNumbers
	  modelKeys: (NSArray *)modelKeys
{
  speciesNames = names;
  speciesIndexes = indexes;
  speciesModelNumbers = modelNumbers;
  speciesModelKeys = modelKeys;
}

- (void)didTransferDataToGPU: (BOOL)aFlag sender: (id)theController
{
  int i, modelIndex;
  NSArray *modelInstances = [theController modelInstances];
  for (modelIndex = 0; modelIndex < [modelInstances count]; ++modelIndex) {
    id modelObject = [modelInstances objectAtIndex: modelIndex];
    for (i = 0; i < [speciesNames count]; ++i) {
      int mnum = [[speciesModelNumbers objectAtIndex: i] intValue];
      int sidx = [[speciesIndexes objectAtIndex: i] intValue];
      id m = [modelObject modelWithNumber: mnum];
      int numOfSpecies = [m numOfSpecies];
      float (*data)[numOfSpecies] = [m dataResults];
      
      printf("instance (%d) species: %s = %f\n", modelIndex, [[speciesNames objectAtIndex: i] UTF8String], (*data)[sidx]);
    }
  }
}
@end

int reaction(int my_rank)
{
  NSAutoreleasePool *pool = [NSAutoreleasePool new];

  BioSwarmManager *simManager = [[BioSwarmManager alloc] initWithDefaultModelFile: MODEL_FILE];
  id delegate = [MyDelegate new];
  [simManager setDelegate: delegate];

  if ([simManager processArguments: [[NSProcessInfo processInfo] arguments]]) {
    [simManager setGPUFunctions: &getGPUFunctionForModel];
    [simManager startSimulation];
  }

  [simManager release];
  [delegate release];

  [pool release];
  return 0;
}

int
main( int argc, char** argv) 
{
  reaction(0);
}
