These are a set of test programs for checking BioSwarm GPU functionality.

Reaction (gpuReaction) -- DEPRECATED
----------------------
Basic reaction model, with static GPU code.
Source--
gpuReaction.m
reaction.bioswarm
Results--
time = 216000
WNT = 0.990175
SHH = 144.685974


Dynamic Reaction (gpuDynamicReaction)
-------------------------------------
Basic reaction model but with dynamic GPU code.
Source--
gpuDynamicReaction.m
dreaction.bioswarm
Results--
./gpuDynamicReaction
time = 216000
instance (0) species: WNT = 0.993181
instance (0) species: SHH = 145.174835


ReactionRule (gpuRule)
----------------------
Newer reaction rule model with support beyond Hill interactions.
This model is functionally equivalent to Reaction model (gpuReaction) above.
Source--
gpuRule.m
rule.bioswarm
Results--
./gpuRule
time = 21600.000000
instance (0) species: WNT = 0.993181
instance (0) species: SHH = 145.174835


Markov chain Monte Carlo (gpuMCMC)
----------------------------------
Basic MCMC for simple single reaction model.
Source--
gpuMCMC.m
mcmc.bioswarm
MCMC.R
mcmcData1.txt
Results--
./gpuMCMC --mcmc
Running R script (MCMC.R) on the resultant data file (MCMC_hist.dat)
should produce a histogram similar to MCMC.pdf


Sequential MCMC Sampler (gpuSMCSampler)
---------------------------------------
Sequential MCMC sampler for simple single reaction model.
Source--
gpuSMCSampler.m
smc.bioswarm
smcHist.R
mcmcData1.txt
Results--
./gpuSMCSampler --mcmc
Running R script (smcHist.R) on the resultant data file (smc_exp.txt)
should produce a histogram similar to SMCSampler.pdf


Medium-sized Reaction Rule Model (gpuMedium)
--------------------------------------------
Medium-sized reaction rule model that is close to the limit of available
registers in the GPU.
Source--
gpuMedium.m
medium.bioswarm
Results--
./gpuMedium
time = 5000.000000
model (0): amet = 1.000000 5.107119
model (0): anth = 1.000000 5.107119
model (0): Pi = 100.000000 510.711914
model (0): lasR = 8.452209 43.166435
model (0): LASR = 7.736071 39.509033
model (0): lasI = 0.940091 4.801159
model (0): LASI = 4.845651 24.747316
model (0): N3OXHSL = 0.051961 0.265373
model (0): LASR_N3OXHSL = 0.488891 2.496825
model (0): LASR_N3OXHSL_dimer = 0.717050 3.662058
model (0): rsaL = 8.452209 43.166435
model (0): RSAL = 8.941982 45.667770
model (0): rhlR = 8.452209 43.166435
model (0): RHLR = 7.697483 39.311962
model (0): rhlI = 8.452209 43.166435
model (0): RHLI = 8.941982 45.667770
model (0): C4HSL = 0.054859 0.280173
model (0): RHLR_C4HSL = 0.446260 2.279101
model (0): RHLR_C4HSL_dimer = 0.398292 2.034126
model (0): RHLR_N3OXHSL = 0.399975 2.042722
model (0): phoB_p = 0.099009 0.505652
model (0): mvfR = 3.630847 18.543169
model (0): MVFR = 6.188674 31.606298
model (0): pqsABCD = 4.952520 25.293112
model (0): PQSA = 8.319991 42.491188
model (0): HHQ = 0.117645 0.600829
model (0): pqsH = 8.452209 43.166435
model (0): PQSH = 8.941982 45.667770
model (0): PQS = 0.015820 0.080793
model (0): MVFR_HHQ = 1.456130 7.436628
model (0): MVFR_PQS = 0.195805 1.000000


Medium-sized Dynamic Reaction Rule Model (gpuDynamicMedium)
-----------------------------------------------------
Medium-sized dynamic reaction rule model.  This model
shares the exact same source as gpuMedium to test that
the static and dynamic models operate equivalently.
Source--
gpuMedium.m
dmedium.bioswarm
Results--


Reaction-diffusion (gpuRD)
--------------------------
Basic two-component reaction diffusion.
Source--
gpuRD.m
rd.swarm
Results--
time = 216000
WNT min = 0.003351, max = 4.084423
SHH min = 138.353714, max = 277.309662
Can display spatial pattern in Matlab with drawTongue.m


Subcellular element method (gpuScEM)
------------------------------------
This uses parameters that give space filling behavior (i.e. Newman's settings).
gpuScEM.m
scem.swarm

Subcellular element method (gpuSEM)
-----------------------------------
This uses parameters that push all the elements to the cell surface.
gpuSEM.m
sem.swarm

Swarm growth
------------
ScEM GPU code integrated with Swarm scheduler.
swarmGrowth.m
growth.swarm

ScEM with basement membrane
---------------------------
ScEM with adhesion to basement membrane.
gpuBM.m
bm.swarm

Optimized ScEM
--------------
Development code to optimize ScEM with element interaction list.
Once working can integrate into BioSwarm.
newSEM.m

