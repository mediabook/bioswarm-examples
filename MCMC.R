#
# plot the resultant histogram from gpuMCMC
#

# these are the max and min parameter range from mcmc.bioswarm
c.min = 0.001
c.max = 0.004

mcmcHist = read.table('MCMC_hist.dat')
mcmcHist.dim = dim(mcmcHist)

# take last row of data
mcmcHist.data = as.numeric(mcmcHist[mcmcHist.dim[1],])

plot(seq(c.min, c.max, length=mcmcHist.dim[2]), mcmcHist.data, xlab = "r1_k", ylab = "Frequency", type='l')
